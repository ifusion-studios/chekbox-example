//
//  AppDelegate.h
//  ChekBox
//
//  Created by Jaha Rabari on 23/08/2012.
//  Copyright (c) 2012 __Jaha Rabari__. All rights reserved.
//  You can get More Example From http://iphonejaha.wordpress.com/
#import <UIKit/UIKit.h>

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController *viewController;

@end
