//
//  ToggleButton.m
//  Directory_iOS
//
//  Created by Jaha Rabari on 5/15/12.
//  Copyright (c) 2012 __Jaha Rabari__. All rights reserved.
//  You can get More Example From http://iphonejaha.wordpress.com/

#import "CheckBox.h"

@implementation CheckBox


-(void)deselect
{
    self.selected = FALSE;
}

-(void)select
{
    self.selected = TRUE;
}

#pragma mark - Properties -

@synthesize selectedState = _selectedState;
@synthesize deselectedState = _deselectedState;

-(void)setSelectedState:(UIButton *)selectedState
{
    [selectedState removeTarget:self action:@selector(deselect) forControlEvents:UIControlEventTouchUpInside];
        
    _selectedState = selectedState;
    _selectedState.hidden = !_selected;
    [_selectedState addTarget:self action:@selector(deselect) forControlEvents:UIControlEventTouchUpInside];
}

-(void)setDeselectedState:(UIButton *)deselectedState
{
    [deselectedState removeTarget:self action:@selector(select) forControlEvents:UIControlEventTouchUpInside];
   
    
    _deselectedState = deselectedState;
    _deselectedState.hidden = _selected;
    [_deselectedState addTarget:self action:@selector(select) forControlEvents:UIControlEventTouchUpInside];
}

-(BOOL)isSelected
{
    return _selected;
}

-(void)setSelected:(BOOL)selected
{
    [self setSelected:selected silent:FALSE];
}

-(void)setSelected:(BOOL)selected silent:(BOOL) silent
{
    if (selected  != _selected)
    {
        if (selected)
        {
            _selected = TRUE;
            _selectedState.hidden = FALSE;
            _deselectedState.hidden = TRUE;
        }
        else
        {
            _selected = FALSE;
            
            _selectedState.hidden = TRUE;
            _deselectedState.hidden = FALSE;
        }
        
        if (!silent)
            [self sendActionsForControlEvents:UIControlEventValueChanged];
    }
}

@end
