//
//  ViewController.h
//  ChekBox
//
//  Created by Jaha Rabari on 23/08/2012.
//  Copyright (c) 2012 __Jaha Rabari__. All rights reserved.
//  You can get More Example From http://iphonejaha.wordpress.com/

#import <UIKit/UIKit.h>
#import "CheckBox.h"
@interface ViewController : UIViewController{
    CheckBox *_chekBox;
}

@property (retain, nonatomic) IBOutlet UILabel *lblStatus;
@end
